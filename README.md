# HTTP Benchmarking

This is obviously a incredibly simple example benchmark that should never be used for any real purpose other than to get an idea of varous concepts in the Go language.

`go run bench.go`

You can change the URI or duration of the benchmark at the end of main(). Results are written to `results.csv`.
