package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

// ErroredResult is the data type used to house an errored request.
func ErroredResult(err error) HTTPBenchmarkResult {
	duration, _ := time.ParseDuration("0s")

	return HTTPBenchmarkResult{err, &http.Response{}, duration}
}

// HTTPBenchmarkResult is the data type used to house our HTTP request data.
type HTTPBenchmarkResult struct {
	err      error
	resp     *http.Response
	duration time.Duration
}

// NewHTTPBenchmarkResult returns a new HTTP benchmark result.
func NewHTTPBenchmarkResult(resp *http.Response, duration time.Duration) HTTPBenchmarkResult {
	return HTTPBenchmarkResult{nil, resp, duration}
}

// MarshalReportLine returns a byte array containing the data from the HTTP benchmark result.
func (br *HTTPBenchmarkResult) MarshalReportLine() []string {
	return []string{strconv.Itoa(br.resp.StatusCode), br.duration.String()}
}

// RequestURI makes an HTTP request and returns a HTTPBenchmarkResult.
func RequestURI(uri string) HTTPBenchmarkResult {
	// Get the time the request is actually made.
	start := time.Now()
	// Initialize a GET request.
	resp, err := http.Get(uri)
	if err != nil {
		log.Printf("%s", err)
		return ErroredResult(err)
	}

	// At this point, the result can be considered a valid response.
	end := time.Now()
	duration := end.Sub(start)

	return NewHTTPBenchmarkResult(resp, duration)
}

func main() {
	uri := "https://gitlab.com"
	duration, err := time.ParseDuration("5m")
	if err != nil {
		log.Panic("Invalid duration")
	}

	fmt.Printf("Benchmarking %s for %.0f seconds\n", uri, duration.Seconds())

	start := time.Now()
	var count int

	f, err := os.OpenFile("results.csv", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		log.Panic(err)
	}
	defer f.Close()
	w := csv.NewWriter(f)

	for time.Now().Sub(start) < duration {
		result := RequestURI(uri)
		fmt.Printf("\rBenchmarked: %d requests", count)
		err := w.Write([]string(result.MarshalReportLine()))
		if err != nil {
			log.Panic(err)
		}
		// Write any buffered data.
		w.Flush()
		// Get any error that might have occurred.
		err = w.Error()
		if err != nil {
			log.Panic(err)
		}
		count++
	}

}
